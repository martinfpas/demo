import React, {Component} from 'react';
import {connect} from 'react-redux';
import {StackNavigator,addNavigationHelpers} from 'react-navigation';
import {createReduxBoundAddListener,createReactNavigationMiddleware} from 'react-navigation-redux-helpers';

import P1 from '../screens/p1'
import Counter from '../screens/Counter'

const Navigator = new StackNavigator({
  PUno: {screen:P1},
  Contador: {screen:Counter},
},{
  InitialRouteName: 'PUno',
})

class Nav extends Component {
  render(){
    return (
        <Navigator navigation={
          addNavigationHelpers({
            dispatch: this.props.dispatch,
            state: this.props.navigation,
          })
        } />
    )

  }
}

const mapStateToProps = (state) => {
  console.log('State:');
  console.log(state);

  return state;

}

export default connect(mapStateToProps)(Nav)
