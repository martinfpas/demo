// Imports: Dependencies
import React from 'react';
import { Provider } from 'react-redux';

// Imports: Screens
import Navigator from './components/Navigator';

// Imports: Redux Store
import { store } from './store/store';

// React Native App
export default function App() {
  return (
    // Redux: Global Store
    <Provider store={store}>
      <Navigator />
    </Provider>
  );
}
