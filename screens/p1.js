import React,{Component} from 'react';
import {View, Text, StyleSheet,TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';

class P1 extends React.Component {
  static navigationOptions = ({navigation}) => ({
    title: 'Pantalla simple. Poner param',
  })

  render() {
      return(
        <View>
          <Text>
            hoLA mUNDO
          </Text>
          <TouchableOpacity onPress={this.props.TextoTocable}>
            <Text>Texto tocable...</Text>
          </TouchableOpacity>
        </View>
      )
  }

}

const mapStateToProps = (state) => {
  console.log('State:');
  console.log(state);

  return state;

}

const mapDispatchToProps = (dispatch) => {
  // esto es una accion
  return {
    TextoTocable: () => dispatch({
      type: 'TEXTO_TOCABLE',
      value: '',
    }),
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(P1);
